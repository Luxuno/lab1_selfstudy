import 'dart:io';

import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import '../l10n/messages_all.dart';
import 'dart:async';

class MenuLocalizations {

  static Future<MenuLocalizations> load(Locale locale) {
    final String lang = locale.countryCode.isEmpty ? locale.languageCode : locale.toString();
    final String localeName = Intl.canonicalizedLocale(lang);
    return initializeMessages(localeName).then(
        (_) {
          Intl.defaultLocale = localeName;
          return MenuLocalizations();
        }
    );
  }


  static MenuLocalizations of(BuildContext context) {
    return Localizations.of<MenuLocalizations>(context, MenuLocalizations);
  }
/*
  static Map<String, Map<String,String>> localizedValues = {
    'en': {
      'title': 'Lab One Application',
      'button': 'Change Language!',
      'desc': 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor',
    },
    'es': {
      'title': 'Aplicaciones del laboratorio uno',
      'button': 'Cambiar el idioma',
      'desc': 'Spanish Text Here',
    },
  };
*/
  String get title {
    return Intl.message('PlaceHolder Title', name: 'title', desc: 'example text');
  }
  String get button {
    return Intl.message('PlaceHolder Button!', name: 'button', desc: 'example text');
  }
  String get desc {
    return Intl.message('PlaceHolder Test Msg', name: 'desc', desc: 'example text');
  }
  String get menu {
    return Intl.message('See Menu', name: 'menu', desc: 'example text');
  }
  String get eggs {
    return Intl.message('Scrambled Eggs', name: 'eggs', desc: 'example text');
  }
  String get eggs_desc {
    return Intl.message("Delicious scrambled eggs "
        "cooked with diced tomatoes, onions, and jalapeños. Gives a great aroma "
        "to the house in the morning. A simple yet yummy meal anyone can enjoy "
        "as their first meal of the day or maybe even lunch.",
        name: 'eggs_desc', desc: 'example text');
  }
  String get soup {
    return Intl.message('Chicken Soup', name: 'soup', desc: 'example text');
  }
  String get soup_desc {
    return Intl.message("Perfect for a cold "
        "winter day or whenever you are feeling under the weather. Its warmth "
        "melts the coldness right out of you. Loaded with a variety of "
        "ingredients and flavors, this dish will definitely be one to remember.",
        name: 'soup_desc', desc: 'example text');
  }
  String get rice {
    return Intl.message('Rice', name: 'rice', desc: 'example text');
  }
  String get rice_desc {
    return Intl.message("Soft and fluffy mexican "
        "rice goes good with almost any mexican dish. With a pleasant orange "
        "color and great aroma it will have any nearby observer wanting a taste."
        " A great side dish with carne asada, berria, or even beans.",
        name: 'rice_desc', desc: 'example text');
  }
  String get select {
    return Intl.message('Select Language', name: 'select', desc: 'example text');
  }
  String get english {
    return Intl.message('English', name: 'english', desc: 'example text');
  }
  String get spanish {
    return Intl.message('Spanish', name: 'spanish', desc: 'example text');
  }
}

class MenuLocalizationsDelegate extends LocalizationsDelegate<MenuLocalizations> {
  final Locale overriddenLocale;

  const MenuLocalizationsDelegate(this.overriddenLocale);

  @override
  bool isSupported(Locale locale) => ['en', 'es'].contains(locale.languageCode);

  @override
  Future<MenuLocalizations> load(Locale locale) => MenuLocalizations.load(locale);

  @override
  bool shouldReload(LocalizationsDelegate<MenuLocalizations> old) => false;
}