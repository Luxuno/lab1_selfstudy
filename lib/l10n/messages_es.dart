// DO NOT EDIT. This is code generated via package:intl/generate_localized.dart
// This is a library that provides messages for a es locale. All the
// messages from the main program should be duplicated here with the same
// function name.

// Ignore issues from commonly used lints in this file.
// ignore_for_file:unnecessary_brace_in_string_interps, unnecessary_new
// ignore_for_file:prefer_single_quotes,comment_references, directives_ordering
// ignore_for_file:annotate_overrides,prefer_generic_function_type_aliases
// ignore_for_file:unused_import, file_names

import 'package:intl/intl.dart';
import 'package:intl/message_lookup_by_library.dart';

final messages = new MessageLookup();

final _keepAnalysisHappy = Intl.defaultLocale;

typedef String MessageIfAbsent(String messageStr, List<dynamic> args);

class MessageLookup extends MessageLookupByLibrary {
  String get localeName => 'es';

  final messages = _notInlinedMessages(_notInlinedMessages);
  static _notInlinedMessages(_) => <String, Function> {
    "title" : MessageLookupByLibrary.simpleMessage("Aplicaciones del laboratorio uno"),
    "button" : MessageLookupByLibrary.simpleMessage("Cambiar el idioma"),
    "desc" : MessageLookupByLibrary.simpleMessage("SPAN_desc"),
    "menu" : MessageLookupByLibrary.simpleMessage("Ver el menú"),
    "eggs" : MessageLookupByLibrary.simpleMessage("Huevos Revueltos"),
    "eggs_desc" : MessageLookupByLibrary.simpleMessage("Deliciosos huevos revueltos "
        "cocinados con tomates, cebollas y jalapeños picados. Da un gran aroma a "
        "la casa por las mañanas. Una comida sencilla pero deliciosa que cualquiera "
        "puede disfrutar como primera comida del día o incluso como almuerzo."),
    "soup" : MessageLookupByLibrary.simpleMessage("Caldo de Pollo"),
    "soup_desc" : MessageLookupByLibrary.simpleMessage("Perfecto para un día frío "
        "de invierno o cuando se sienta mal. Su calidez derrite la frialdad de ti. "
        "Cargado con una variedad de ingredientes y sabores, este plato definitivamente "
        "será uno para recordar."),
    "rice" : MessageLookupByLibrary.simpleMessage("Arroz"),
    "rice_desc" : MessageLookupByLibrary.simpleMessage("El arroz mexicano suave y "
        "esponjoso combina bien con casi cualquier plato mexicano. Con un agradable "
        "color naranja y un gran aroma, hará que cualquier observador cercano quiera "
        "probarlo."),
    "select" : MessageLookupByLibrary.simpleMessage("Seleccione el idioma"),
    "english" : MessageLookupByLibrary.simpleMessage("Inglés (English)"),
    "spanish" : MessageLookupByLibrary.simpleMessage("Español")
  };
}
