import 'dart:io';

import 'package:flutter/foundation.dart' show SynchronousFuture;
import 'package:flutter/material.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'locale/MenuLocalizations.dart';

enum Food { Scrambled_Eggs, Chicken_Soup, Rice }
enum Locales { English, Spanish }

void main() {
  runApp(MyApp());
}

class MyApp extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => MyAppState();
}

class MyAppState extends State<MyApp> {
  // This widget is the root of your application.

  MenuLocalizationsDelegate _localeOverrideDelegate =
  MenuLocalizationsDelegate(Locale('en', 'US'));
  @override

  Widget build(BuildContext context) {

    return MaterialApp(
      localizationsDelegates: [
        GlobalMaterialLocalizations.delegate,
        GlobalWidgetsLocalizations.delegate,
        _localeOverrideDelegate
      ],
      supportedLocales: [
        const Locale('en', 'US'),
        const Locale('es', 'MX'),
      ],
      debugShowCheckedModeBanner: false,
      title: 'Menu',
      home: MenuHomePage(),
    );
  }
}

class _languagebarrier extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => _languagebarrierstate();
}

class _languagebarrierstate extends State<_languagebarrier> {
  int count = 0;

  @override
  Widget build(BuildContext context) {
    //Locale myLocale = Localizations.localeOf(context);
    //print('-1/7. ' + myLocale.toString());
    count = 0;
    return Align(
      alignment: Alignment.bottomCenter,
      child: Column(
        children: [
          RaisedButton(color: Colors.pinkAccent, textColor: Colors.white,
            child: Text(MenuLocalizations.of(context).button),
            onPressed: () {
              setState(() {
                MenuHomePageState().initState();
                Locale myLocale = Localizations.localeOf(context);
                MenuLocalizations.load(Locale('es','MX'));
              });
            },
          ),
          RaisedButton(color: Colors.pinkAccent, textColor: Colors.white,
            child: Text(MenuLocalizations.of(context).button),
            onPressed: () {
              setState(() {
                MenuHomePageState().initState();
                Locale myLocale = Localizations.localeOf(context);
                MenuLocalizations.load(Locale('en','US'));
              });
            },
          ),
        ],
      )
      /*RaisedButton(color: Colors.pinkAccent, textColor: Colors.white,
        child: Text(MenuLocalizations.of(context).button),
        onPressed: () {

          print('1. Test');

          setState(() {
            Locale myLocale = Localizations.localeOf(context);

            print('2. ' + myLocale.toString());
            print('2.5 ' + Platform.localeName);

            if(myLocale.toString() == 'en_US') {
              print('3. ENG -> SPAN');
              MenuLocalizations.load(Locale('es','MX'));
            }
            if(myLocale.toString() == 'es_MX') {
              print('3. SPAN -> ENG');
              MenuLocalizations.load(Locale('en','US'));
            }
          });

        },
      ),
      */
    );
  }
}

class MenuHomePage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => MenuHomePageState();
}

class MenuHomePageState extends State<MenuHomePage> {
  Widget build(BuildContext context) {

    MenuLocalizations ml = MenuLocalizations.of(context);

    Widget theMenu = Stack(
      children: [
        Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              MaterialButton(
                child: Text(ml.menu, style: TextStyle(color: Colors.white),),
                color: Colors.pinkAccent,
                onPressed: _Menu
              ),
              MaterialButton(
                child: Text(ml.button, style: TextStyle(color: Colors.white),),
                color: Colors.pinkAccent,
                onPressed: _LanguageMenu
              ),
            ],
          )
        ),
      ],
    );
   // Add a comment to this line

    return Scaffold(
      appBar: AppBar(
        title: Text(ml.title),
      ),
      body: theMenu,
    );
  }

  Future<void> _LanguageMenu() async {

    switch (await showDialog<Locales>(
        context: context,
        builder: (BuildContext context) {
          return SimpleDialog(
            backgroundColor: Colors.teal,         //<--IF YOU GUYS WANT TO CHANGE THE COLOR OF DIALOG BOX DO SO HERE
            title: new Text(MenuLocalizations.of(context).select,
              style: TextStyle(
                  fontWeight:
                  FontWeight.bold,
                  color: Colors.white),),
            children: <Widget>[
              SimpleDialogOption(onPressed: () {
                setState(() {
                  Locale myLocale = Localizations.localeOf(context);
                  MenuLocalizations.load(Locale('en','US'));
                  Navigator.pop(context);
                });
              },
                child: new Text(MenuLocalizations.of(context).english, style: TextStyle(color: Colors.white),),
              ),
              SimpleDialogOption(onPressed: () {
                setState(() {
                  Locale myLocale = Localizations.localeOf(context);
                  MenuLocalizations.load(Locale('es','MX'));
                  Navigator.pop(context);
                });
              },
                child: new Text(MenuLocalizations.of(context).spanish, style: TextStyle(color: Colors.white),),
              ),
            ],
          );
        }
    )) {
      default:
        break;
    }
  }

  //Used https://api.flutter.dev/flutter/material/SimpleDialog-class.html help with
  //the code for the simple dialog
  Future<void> _Menu() async {
    switch (await showDialog<Food>(
        context: context,
        builder: (BuildContext context) {
          return SimpleDialog(
            backgroundColor: Colors.teal,         //<--IF YOU GUYS WANT TO CHANGE THE COLOR OF DIALOG BOX DO SO HERE
            title: new Text(MenuLocalizations.of(context).menu, style: TextStyle(fontWeight: FontWeight.bold, color: Colors.white),),
            children: <Widget>[
              SimpleDialogOption(
                onPressed: () {
                  Navigator.pop(context, Food.Scrambled_Eggs);
                },
                child: new Text(MenuLocalizations.of(context).eggs, style: TextStyle(color: Colors.white),),
              ),
              SimpleDialogOption(
                onPressed: () {
                  Navigator.pop(context, Food.Chicken_Soup);
                },
                child: new Text(MenuLocalizations.of(context).soup, style: TextStyle(color: Colors.white),),
              ),
              SimpleDialogOption(
                onPressed: () {
                  Navigator.pop(context, Food.Rice);
                },
                child: new Text(MenuLocalizations.of(context).rice, style: TextStyle(color: Colors.white),),
              ),
            ],
          );
        }
    )) {
      case Food.Scrambled_Eggs:
        Navigator.push(
          context,
          MaterialPageRoute(builder: (context) =>
              FoodPage(
                desc: MenuLocalizations.of(context).eggs_desc,
                name: MenuLocalizations.of(context).eggs,
                image: 'assets/SE.jpg',
              ),
          ),
        );
        break;
      case Food.Chicken_Soup:
        Navigator.push(
          context,
          MaterialPageRoute(builder: (context) =>
              FoodPage(
                desc: MenuLocalizations.of(context).soup_desc,
                name: MenuLocalizations.of(context).soup,
                image: 'assets/CS.jpg',
              ),
          ),
        );
        break;
      case Food.Rice:
        Navigator.push(
          context,
          MaterialPageRoute(builder: (context) =>
              FoodPage(
                desc: MenuLocalizations.of(context).rice_desc,
                name: MenuLocalizations.of(context).rice,
                image: 'assets/MR.jpg',
              ),
          ),
        );
        break;
    }
  }
}

class FoodPage extends StatelessWidget {
  String desc;
  String name;
  String image;

  FoodPage(
      {this.desc, this.name, this.image}) {}
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text(name), backgroundColor: Colors.brown,),
      body: Container(
        height: MediaQuery.of(context).size.height,
        width: MediaQuery.of(context).size.width,
        color: Colors.purple,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            Container(
              width: MediaQuery.of(context).size.width,
              height: MediaQuery.of(context).size.height/1.5,
              child:Image.asset(image, fit: BoxFit.cover,),),
            Container(
              padding: const EdgeInsets.only(top: 20, left: 10, right: 10),
              child: Text(desc, style: TextStyle(fontSize: 18, color: Colors.white),),)
          ],
        ),
      ),
    );
  }
}
