// DO NOT EDIT. This is code generated via package:intl/generate_localized.dart
// This is a library that provides messages for a en locale. All the
// messages from the main program should be duplicated here with the same
// function name.

// Ignore issues from commonly used lints in this file.
// ignore_for_file:unnecessary_brace_in_string_interps, unnecessary_new
// ignore_for_file:prefer_single_quotes,comment_references, directives_ordering
// ignore_for_file:annotate_overrides,prefer_generic_function_type_aliases
// ignore_for_file:unused_import, file_names

import 'package:intl/intl.dart';
import 'package:intl/message_lookup_by_library.dart';

final messages = new MessageLookup();

final _keepAnalysisHappy = Intl.defaultLocale;

typedef String MessageIfAbsent(String messageStr, List<dynamic> args);

class MessageLookup extends MessageLookupByLibrary {
  String get localeName => 'en';

  final messages = _notInlinedMessages(_notInlinedMessages);
  static _notInlinedMessages(_) => <String, Function> {
    "title" : MessageLookupByLibrary.simpleMessage("Lab One Application"),
    "button" : MessageLookupByLibrary.simpleMessage("Change Language!"),
    "desc" : MessageLookupByLibrary.simpleMessage("ENG_desc"),
    "menu" : MessageLookupByLibrary.simpleMessage("See Menu"),
    "eggs" : MessageLookupByLibrary.simpleMessage("Scrambled Eggs"),
    "egg_desc" : MessageLookupByLibrary.simpleMessage("Delicious scrambled eggs "
        "cooked with diced tomatoes, onions, and jalapeños. Gives a great aroma "
        "to the house in the morning. A simple yet yummy meal anyone can enjoy "
        "as their first meal of the day or maybe even lunch. "),
    "soup" : MessageLookupByLibrary.simpleMessage("Chicken Soup"),
    "soup_desc" : MessageLookupByLibrary.simpleMessage("Perfect for a cold "
        "winter day or whenever you are feeling under the weather. Its warmth "
        "melts the coldness right out of you. Loaded with a variety of "
        "ingredients and flavors, this dish will definitely be one to remember."),
    "rice" : MessageLookupByLibrary.simpleMessage("Rice"),
    "rice_desc" : MessageLookupByLibrary.simpleMessage("Soft and fluffy mexican "
        "rice goes good with almost any mexican dish. With a pleasant orange "
        "color and great aroma it will have any nearby observer wanting a taste."
        " A great side dish with carne asada, berria, or even beans."),
    "select" : MessageLookupByLibrary.simpleMessage("Select Language"),
    "english" : MessageLookupByLibrary.simpleMessage("English"),
    "spanish" : MessageLookupByLibrary.simpleMessage("Español (Spanish)")
  };
}
